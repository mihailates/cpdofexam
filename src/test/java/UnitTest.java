package com.agiletestingalliance;

import java.io.*;
import org.junit.Test;
import java.lang.String;
import static org.junit.Assert.*;

public class UnitTest {
  @Test
    public void testMinMax1() throws Exception {

        int var = new MinMax().myfunc(1,2);
        assertEquals("min is first",var,2);
        }

         @Test
    public void testMinMax2() throws Exception {

        int var = new MinMax().myfunc(10,9);
        assertEquals("min is second",var,10);

        }


    @Test
    public void testAbout() throws Exception {

        String test = new AboutCPDOF().desc();
        assertTrue(test.contains("CP-DOF certification program covers end to end DevOps Life Cycle practically"));
    }


         @Test
    public void testUsefullness() throws Exception {
         String test = new Usefulness().desc();
         assertTrue(test.contains("helps you learn DevOps fundamentals along with Continuous Integration"));
    }
      
    @Test
         public void testDuration() throws Exception {

        String test = new Duration().dur();
        assertTrue(test.contains("CP-DOF is designed specifically for corporates and working professionals alike"));
    }

}



